#pragma once

#include "transform.h"

enum transform_status rotate(const struct image source, struct image* output);
