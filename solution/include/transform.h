#pragma once

enum transform_status {
    TRANSFORM_OK = 0,
    TRANSFORM_MEMORY_ENDED,
    TRANSFORM_ERROR
};
