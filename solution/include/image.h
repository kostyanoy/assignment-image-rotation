#pragma once

#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* pixels;
};

struct pixel* allocate_pixels(uint64_t height, uint64_t width);
struct image image_create(uint64_t height, uint64_t width, struct pixel* pixels);
void image_destroy(struct image image);
struct pixel* image_get_pixel(const struct image image, uint64_t x, uint64_t y);
