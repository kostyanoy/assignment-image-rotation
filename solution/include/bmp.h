#pragma once

#include <stdint.h>
#include <stdio.h>

#include "image.h"

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)

enum bmp_read_status {
    BMPR_OK = 0,
    BMPR_INVALID_SIGNATURE,
    BMPR_UNSUPPORTED_BITS,
    BMPR_INVALID_HEADER,
    BMPR_MEMORY_ENDED,
    BMPR_ERROR
};
enum bmp_write_status {
    BMPW_OK = 0,
    BMPW_HEADER,
    BMPW_DATA,
    BMPW_PADDING,
    BMPW_ERROR
};

enum bmp_read_status bmp_to_image(struct image* image, FILE* file);
enum bmp_write_status bmp_from_image(const struct image* image, FILE* file);
