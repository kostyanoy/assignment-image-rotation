#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "transform.h"
#include "utils.h"

// error mesages
static const char* bmp_read_output[] = {
    "Read successfull!",
    "Signature invalid! It is not BMP file",
    "Unsupported bits! BMP must be 24 bits per pixel",
    "Invalid header! Probably offset",
    "Memory error! Can't save pixels",
    "Some read error!"
};

static const char* image_transform_output[] = {
    "Transform successfull!",
    "Memory error! Can't allocate memory for transformed image",
    "Some transform error"
};

static const char* bmp_write_output[] = {
    "Write successfull!",
    "Error while writing header",
    "Error while writing data",
    "Error while writing padding",
    "Some writing error"
};

static const char* READ_BINARY_MODE = "rb";
static const char*  WRITE_BINARY_MODE = "wb";

// process file
static int process(FILE* in, FILE* out){
    struct image image = {0};
    struct image tranformed_image = {0};

    // bmp to image
    const enum bmp_read_status r_status = bmp_to_image(&image, in);
    if (r_status) {
        print_error(bmp_read_output[r_status]);
        return 1;
    }
    print_message(bmp_read_output[r_status]);

    // transform image
    const enum transform_status t_status = rotate(image, &tranformed_image);
    if (t_status) {
        print_error(image_transform_output[t_status]);
        return 1;
    }
    print_message(image_transform_output[t_status]);
    
    // image to bmp
    const enum bmp_write_status w_status = bmp_from_image(&tranformed_image, out);
    if (w_status) {
        print_error(bmp_write_output[w_status]);
        return 1;
    }
    print_message(bmp_write_output[r_status]);


    // clear memory
    image_destroy(image);
    image_destroy(tranformed_image);

    return 0;
}

int main( int argc, char** argv ) {
    // check amount of parameters
    if (argc != 3){
        print_error("Not enough parameters");
        return 1;
    }

    // open input file
    FILE* input_file = fopen(argv[1], READ_BINARY_MODE); 
    if (!input_file){
        print_error("Can't open input file");
        return 1;
    }

    // open output file 
    FILE* output_file = fopen(argv[2], WRITE_BINARY_MODE);
    if (!output_file){
        print_error("Can't open output file");
        fclose(input_file);
        return 1;
    }

    // process file 
    const int exit_code = process(input_file, output_file);

    // close files
    fclose(input_file);
    fclose(output_file);

    return exit_code;
}

