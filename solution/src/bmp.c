#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "image.h"

// bmp constants
static const uint16_t BYTES_IN_PIXEL = 3; // 24 bits == 3 bytes
static const uint16_t BMP_BIT_COUNT = BYTES_IN_PIXEL * 8;
static const uint8_t BMP_PADDING = 4;
static const uint16_t BMP_TYPE = 0x4D42;
static const uint32_t BMP_INFO_SIZE = 40;
static const uint16_t BMP_PLANES = 1;

// calculate padding from image width in bytes
static uint8_t calc_padding(uint32_t byte_width, uint8_t padding) {
    const uint8_t mod = byte_width % padding;
    return (mod == 0) ? 0 : padding - mod;
}

// get pixels
static struct pixel* read_pixels(FILE* file, uint32_t height, uint32_t width) {
    struct pixel* pixels = allocate_pixels(height, width);
    const uint32_t image_width = width * BYTES_IN_PIXEL; // pixels
    const uint8_t padding = calc_padding(image_width, BMP_PADDING); // padding
    if (!pixels){
        return NULL; // can't allocate memory
    }
    for (uint32_t i = 0; i < height; i++)
    {
        if (!fread(&pixels[i * width], image_width, 1, file) ||
            fseek(file, padding, 1) // skip padding
        )
        {
            free(pixels);
            return NULL;
        }
    }
    return pixels;
}

// generate bmp_header from image struct 
static struct bmp_header header_from_image(const struct image* image) {
    struct bmp_header header = {0};
    const uint8_t padding = calc_padding(image->width * BYTES_IN_PIXEL, BMP_PADDING);
    const uint32_t row_width = image->width + padding;
    const uint32_t data_size = image->height * row_width;

    // make header
    header.bfType = BMP_TYPE;
    header.bfileSize = sizeof(struct bmp_header) + data_size;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_INFO_SIZE;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = BMP_BIT_COUNT;
    header.biSizeImage = data_size;

    return header;
}

// validate bmp_header
static enum bmp_read_status check_header(const struct bmp_header header) {
    if (header.bfType != BMP_TYPE){
        return BMPR_INVALID_SIGNATURE; // must be a bmp file
    }
    if (header.biBitCount != BMP_BIT_COUNT){
        return BMPR_UNSUPPORTED_BITS; // must be 24 bits
    }
    if (header.biHeight <= 0 || header.biWidth <= 0 || header.bOffBits < sizeof(struct bmp_header)){
        return BMPR_INVALID_HEADER; // height and width must be >= 0, offbits >= 54
    }
    return BMPR_OK;
}

// get image struct from bmp file
enum bmp_read_status bmp_to_image(struct image* image, FILE* file){
    // read header
    struct bmp_header header = {0}; 
    if (!fread(&header, sizeof(struct bmp_header), 1, file)){
        return BMPR_ERROR; // can't read
    }

    // check header
    const enum bmp_read_status check_status = check_header(header);
    if (check_status) {
        return check_status;
    }

    // header.bOffBits >= sizeof(struct bmp_header) for sure here
    // skip bits if offset is bigger than header
    const long header_size = sizeof(struct bmp_header);
    if(fseek(file, (long)header.bOffBits - header_size, 1)) {
        return BMPR_ERROR;
    }

    // read pixels
    struct pixel* pixels = read_pixels(file, header.biHeight, header.biWidth);
    if (!pixels){
        return BMPR_MEMORY_ENDED;
    }

    image->height = header.biHeight;
    image->width = header.biWidth;
    image->pixels = pixels;

    return BMPR_OK;
}

// get bmp file from image struct
enum bmp_write_status bmp_from_image(const struct image* image, FILE* file){
    const struct bmp_header header = header_from_image(image);
    const uint8_t padding = calc_padding(image->width * BYTES_IN_PIXEL, BMP_PADDING);

    // write header
    if (!fwrite(&header, sizeof(struct bmp_header), 1, file)) {
        return BMPW_HEADER;
    }
    for (uint64_t i = 0; i < image->height; i++){
        // write data
        if (!fwrite(&image->pixels[i * image->width], image->width, BYTES_IN_PIXEL, file)) {
            return BMPW_DATA;
        }
        // write padding
        if (!fwrite("1234", padding, 1, file)){
            return BMPW_PADDING;
        }

    }

    return BMPW_OK;
}

