#include <stdio.h>

// print error to stderr
void print_error(const char* str){
    fprintf(stderr, "%s\n", str);
}

// print message to stdout
void print_message(const char* str){
    fprintf(stdout, "%s\n", str);
}
