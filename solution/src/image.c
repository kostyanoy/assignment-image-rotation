#include <stdint.h>
#include <stdlib.h>

#include "image.h"

// allocate memory for pixels
struct pixel* allocate_pixels(uint64_t height, uint64_t width) {
    return malloc(sizeof(struct pixel) * height * width);
}

// create image struct
struct image image_create(uint64_t height, uint64_t width, struct pixel* pixels){
    return (struct image) {.height = height, .width = width, .pixels = pixels};
}

// destroy allocated pixels
void image_destroy(struct image image){
    free(image.pixels);
}

// get pixel address by x, y
struct pixel* image_get_pixel(const struct image image, uint64_t x, uint64_t y){
    if (x < 0 || y < 0 || x >= image.width || y >= image.height){
        return NULL; // wrong arguments
    }
    return &image.pixels[y * image.width + x]; // x - width, y - height
}
