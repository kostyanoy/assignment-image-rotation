#include <stdint.h>

#include "image.h"
#include "transform.h"

// init sctuct for image rotation result
static enum transform_status init_result( const struct image source, struct image* output ) {
    output->height = source.width;
    output->width = source.height;

    struct pixel* pixels = allocate_pixels(output->height, output->width);

    if (!pixels) {
        return TRANSFORM_MEMORY_ENDED;
    }

    output->pixels = pixels;
    return TRANSFORM_OK;
}

// rotate for 90 degrees anti-clockwise
enum transform_status rotate(const struct image source, struct image* output){
    const enum transform_status init_status = init_result(source, output);
    if (init_status){
        return init_status;
    }

    // transform pixels
    for (uint64_t i = 0; i < source.height; i++){
        for(uint64_t j = 0; j < source.width; j++){
            const uint64_t new_i = j;
            const uint64_t new_j = source.height - i - 1;

            output->pixels[new_i * output->width + new_j] = *image_get_pixel(source, j, i);
        }
    }

    return TRANSFORM_OK;
}
